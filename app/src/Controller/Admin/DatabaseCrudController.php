<?php

namespace App\Controller\Admin;

use App\Entity\Database;
use App\Service\Interfaces\IOpenSSLEncryptionService;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class DatabaseCrudController extends AbstractCrudController
{
    private $encryptionService;

    public function __construct(IOpenSSLEncryptionService $encryptionService)
    {
        $this->encryptionService = $encryptionService;
    }

    public static function getEntityFqcn(): string
    {
        return Database::class;
    }

    /**
     * @Override 
     * 
     * @param EntityManagerInterface $entityManager 
     * @param mixed $entityInstance 
     * @return void 
     */
    public function persistEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setPassword($this->encryptionService->encrypt($entityInstance->getPassword()));
        parent::updateEntity($entityManager, $entityInstance);
    }

    /**
     * @Override 
     * 
     * @param EntityManagerInterface $entityManager 
     * @param mixed $entityInstance 
     * @return void 
     */
    public function updateEntity(EntityManagerInterface $entityManager, $entityInstance): void
    {
        $entityInstance->setPassword($this->encryptionService->encrypt($entityInstance->getPassword()));
        parent::updateEntity($entityManager, $entityInstance);
    }

    //display fields of the CRUD
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('login'),
            TextField::new('password', 'Password')
                ->hideOnDetail()
                ->setFormType(PasswordType::class),
            TextField::new('password', 'Encrypted password size')
                ->onlyOnIndex()
                ->formatValue(fn ($value, $entity) => strlen($value) . ' characters'),
            TextField::new('password', 'Password (decrypted)')
                ->onlyOnIndex()
                ->formatValue(fn ($value, $entity) => $this->encryptionService->decrypt($value)),
        ];
    }
}
