<?php

namespace App\Service\Interfaces;

interface IOpenSSLEncryptionService extends IStringEncryptionService
{
    /**
     * Set the value of cipherAlgorithm
     *
     * @return  self
     */
    public function setCipherAlgorithm(string $cipherAlgorithm): IOpenSSLEncryptionService;
}
