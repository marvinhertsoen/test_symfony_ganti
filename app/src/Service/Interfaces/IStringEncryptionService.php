<?php

namespace App\Service\Interfaces;

interface IStringEncryptionService
{
    /**
     * Encrypt a string
     * 
     * @param string $str 
     * @return string 
     */
    public function encrypt(string $str): string;

    /**
     * Decrypt a string
     * 
     * @param string $str 
     * @return string 
     */
    public function decrypt(string $str): string;
}
