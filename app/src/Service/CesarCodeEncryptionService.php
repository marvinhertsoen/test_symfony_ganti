<?php

namespace App\Service;

use App\Service\Interfaces\IStringEncryptionService;

class CesarCodeEncryptionService implements IStringEncryptionService
{
    /**
     * Encrypt a string
     * 
     * @param string $str 
     * @return string 
     */
    public function encrypt(string $str): string
    {
        $str = str_split($str);
        $newStr = '';
        foreach ($str as $char) {
            $newStr .= chr(ord($char) + 3);
        }
        return $newStr;
    }

    /**
     * 
     * @param string $str 
     * @return string 
     */
    public function decrypt(string $str): string
    {
        $str = str_split($str);
        $newStr = '';
        foreach ($str as $char) {
            $newStr .= chr(ord($char) - 3);
        }
        return $newStr;
    }
}
