<?php

namespace App\Service;

use App\Service\Interfaces\IOpenSSLEncryptionService;

abstract class AbstractOpenSSLEncryptionService implements IOpenSSLEncryptionService
{
    const ENV_ENCRYPTION_KEY = 'ENCRYPTION_PASSWORD';
    const DEFAULT_ENCRYPTION_KEY = 'default_key';

    const CIPHER_ALGORITHM = '';

    protected string $encryptionKey;
    protected string $cipherAlgorithm;

    public function __construct()
    {
        $this->encryptionKey = $_ENV[self::ENV_ENCRYPTION_KEY] ?? self::DEFAULT_ENCRYPTION_KEY;
        $this->cipherAlgorithm = static::CIPHER_ALGORITHM; // get the cipher algorithm from the child class
    }

    /**
     * Set the value of cipherAlgorithm
     *
     * @return  self
     */
    public function setCipherAlgorithm(string $cipherAlgorithm): IOpenSSLEncryptionService
    {
        $this->cipherAlgorithm = $cipherAlgorithm;
        return $this;
    }

    /**
     * Encrypt a string 
     * 
     * @param string $str 
     * @return string 
     */
    public function encrypt(string $str): string
    {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->getCipherAlgorithm()));
        $encrypted = openssl_encrypt($str, $this->getCipherAlgorithm(), $this->encryptionKey, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
    }

    /**
     * Decrypt a string
     * 
     * @param mixed $str 
     * @return string|false 
     */
    public function decrypt($str): string
    {
        list($encrypted, $iv) = explode('::', base64_decode($str), 2);
        return openssl_decrypt($encrypted, $this->getCipherAlgorithm(), $this->encryptionKey, 0, $iv);
    }

    /**
     * Get the value of cipherAlgorithm

     * @return string 
     */
    public function getCipherAlgorithm(): string
    {
        if (!$this->cipherAlgorithm) {
            throw new \Exception('Cipher algorithm not set');
        }

        return $this->cipherAlgorithm;
    }
}
