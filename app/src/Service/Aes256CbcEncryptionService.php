<?php

namespace App\Service;

class Aes256CbcEncryptionService extends AbstractOpenSSLEncryptionService
{
    const CIPHER_ALGORITHM = 'aes-256-cbc';
}
