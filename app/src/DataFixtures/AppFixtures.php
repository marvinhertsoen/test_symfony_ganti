<?php

namespace App\DataFixtures;

use App\Entity\Database;
use App\Service\Interfaces\IOpenSSLEncryptionService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private $encryptionService;
    private $faker;

    public function __construct(IOpenSSLEncryptionService $encryptionService)
    {
        $this->encryptionService = $encryptionService;
        $this->faker = \Faker\Factory::create('en_US');
    }
    public function load(ObjectManager $manager): void
    {
        for($i=0; $i<100; $i++){
            $database = new Database();
            
            $userFullName = $this->faker->name;
            
            //first letter of the first name + last name
            $login = strtolower(substr($userFullName, 0, 1) . $this->faker->lastName);

            $database->setName($userFullName)
                    ->setLogin($login)
                    ->setPassword($this->encryptionService->encrypt('password_of_' . $login));
            $manager->persist($database);
        }

        $manager->flush();
    }
}
