# Symfony Technical Test

A simple string encryption application built with Symfony.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

You need to have docker & docker-compose on your machine.

### Installation

1. Clone the repository
```bash
git clone https://gitlab.com/marvinhertsoen/test_symfony_ganti.git
```

2. Navigate into the project directory
```bash
cd test_symfony_ganti
```

3. Run the containers
```bash
docker-compose up -d
```

4. Go inside the php container
```bash
docker-compose exec php bash
```

5. Install the dependencies via composer
```bash
composer install
```

6. Run the migrations 
```bash
bin/console doctrine:migrations:migrate
```

7. Apply the fixtures
```bash
bin/console doctrine:fixtures:load
```

## Usage

- Visit the Databases page at [http://localhost:8080/admin](http://localhost:8080/admin). You can add / edit and delete databases here.

## License

This project is licensed under the MIT License
